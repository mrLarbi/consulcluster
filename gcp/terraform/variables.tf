variable "consul_server_count" {
  type = number
  default = 3
}

variable "region" {
  type = string
  default = "europe-west1-c"
}
