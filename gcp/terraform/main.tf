resource "google_compute_firewall" "consul-firewall" {
  name    = "consul-firewall"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "8600", "8500", "8300", "8301", "8302"]
  }

  allow {
    protocol = "udp"
    ports    = ["8600", "8301", "8302"]
  }

  target_tags = ["consul"]
}

resource "google_compute_instance_group_manager" "consul-cluster" {
  name = "consul-cluster-igm"

  base_instance_name = "consul-server"
  zone               = var.region

  version {
    instance_template  = google_compute_instance_template.consul-server.self_link
  }

  target_size  = var.consul_server_count
}

resource "google_compute_instance_template" "consul-server" {

  name         = "consul-server-template"
  machine_type = "n1-standard-1"

  disk {
    source_image = "consul-server-image"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = ""
    }
  }

  tags = ["consul"]

  service_account {
    scopes = ["compute-ro"]
  }
}
