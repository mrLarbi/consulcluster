variable "consul_server_count" {
  type = number
  default = 3
}

resource "aws_security_group" "" {
  name = ""
  description = ""

  ingress {
    from_port = "22"
    to_port = "22"
    protocol = "tcp"
  }

  ingress {
    from_port = "8500"
    to_port = "8500"
    protocol = "tcp"
  }

  ingress {
    from_port = "8600"
    to_port = "8600"
    protocol = "tcp"
  }

  ingress {
    from_port = "8300"
    to_port = "8302"
    protocol = "tcp"
  }

  ingress {
    from_port = "8301"
    to_port = "8302"
    protocol = "udp"
  }

  ingress {
    from_port = "8600"
    to_port = "8600"
    protocol = "udp"
  }
}

resource "aws_instance" "consul" {
  count = var.consul_server_count

  ami = "ami-0559807e9cefbad8f"
  instance_type = "t2.micro"

  tags = {
    Name = "consul${count.index}"
    ConsulServer = "yes"
  }
}
