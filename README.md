### How to run on GCP

#### Build the image with Packer

- Set the GOOGLE_APPLICATION_CREDENTIALS to the path of you credentials file
- Build the image : 
```
git clone https://gitlab.com/mrLarbi/consulcluster
cd gcp/packer/
packer build -force consul-server.json
```

#### Install the cluster with Terraform

- Set the GOOGLE_CREDENTIALS to the path of you credentials file
- Install the cluster : 
```
cd gcp/terraform
terraform init
terraform apply
```

### How to run on AWS

TODO

### How to run on Azure

TODO